<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class PrincipalController extends Controller {

        public function index() {
            // Logica de control

            // Renderizar vista
            return view('principal.menu');
        }

        public function noticias() {
            // Logica de control
            $noticias = DB::table('noticias')->get();

            // Renderizar vista
            return view('noticias', ['noticias' => $noticias]);
        }
    }
