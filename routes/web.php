<?php
    use App\Http\Controllers\PrincipalController;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Route;

    // Ruta de tipo get "/" => hola "Hola, clase"
    Route::get('/mensaje', function () {
        return "Hola, clase";
    });

    Route::get('/menu', function () {
        $menu = "<ul>
                    <li>item 1</li>
                    <li>item 2</li>
                    <li>item 3</li>
                </ul>";
        
        return $menu;
    });

    // Ruta de tipo get que llama a una vista "/inicio" => renderiza la vista "inicio"
    Route::get('/inicio', function () {
        // Logica de control
        return view('inicio');
    });

    // Ruta para cargar vista "/contacto" => renderiza la vista "contacto"
    Route::view('/contacto', 'contacto');

    Route::get('/listado', function () {
        $dias = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];

        return view('listado', ['dias' => $dias]);
    });

    // Ruta de tipo view "/colores" => renderiza la vista "colores"
    Route::view('/colores', 'colores', ['colores' => ['red', 'blue', 'yellow', 'green']]);

    // Ruta de tipo get "/menu1" => renderiza la vista "menu1". LLamaremos a la accion "PrincialController index"
    Route::get('/', [PrincipalController::class, 'index']);

    // Ruta de tipo get "/noticias" => renderiza la vista "noticias". Lista las noticias de la tabla "noticias"
    Route::get('/noticias', function () {
        // Sacar todas las noticias no tenemos modelo lo voy a realizar con query builder
        $noticias = DB::table('noticias')->get();

        return $noticias;
    });

    // Ruta de tipo get "/noticias" => Lista a la vista "noticias" y la vista noicias me mostrara
    // y llama a la vista "noticias" con la url "/noticias1"
    Route::get('/noticias1', function () {
        $noticias = DB::table('noticias')->get();

        return view('noticias', ['noticias' => $noticias]);
    });

    // Ruta de tipo get "/noticias" => LLamar a la accion noticias y saco todoas las noticias de la tabla noticias
    // y llama a la vista "noticias" con la url "/noticias2"
    Route::get('/noticias2', [PrincipalController::class, 'noticias']);