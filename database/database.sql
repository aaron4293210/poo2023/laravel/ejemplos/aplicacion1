use aplicacion1;

CREATE TABLE noticias(  
    id int AUTO_INCREMENT,
    titulo varchar(255),
    texto VARCHAR(255),
    create_time DATETIME,
    update_time DATETIME,
    PRIMARY KEY (id)
);

INSERT INTO noticias 
    (id, titulo, texto)
VALUES
    (1, 'Noticia 1', 'Texto de la noticia 1'),
    (2, 'Noticia 2', 'Texto de la noticia 2'),
    (3, 'Noticia 3', 'Texto de la noticia 3');